import Koa from "koa";
import koaMount from "koa-mount";
import { PlanetService } from "@eternal-twin/galaxy55-api-types/planet/service.js";
import { createPlanetsRouter } from "./planets.js";

export interface Api {
  planet: PlanetService;
}

export function createApiRouter(api: Api): Koa {
  const router: Koa = new Koa();

  router.use(koaMount("/planets", createPlanetsRouter(api)));

  router.use((ctx: Koa.Context) => {
    ctx.response.status = 404;
    ctx.body = {error: "ResourceNotFound"};
  });

  return router;
}
