import { PlanetService } from "@eternal-twin/galaxy55-api-types/planet/service.js";
import Koa from "koa";
import koaRoute from "koa-route";

export interface Api {
  planet: PlanetService;
}

export function createPlanetsRouter(api: Api): Koa {
  const router: Koa = new Koa();

  router.use(koaRoute.get("/:planet_id/users", getUsersByPlanetId));

  async function getUsersByPlanetId(ctx: Koa.Context, rawPlanetId: string): Promise<void> {
    const planetId: string = rawPlanetId; // TODO: Validate
    const users = await api.planet.getPlayersOnPlanet(planetId);
    ctx.response.body = JSON.stringify(users);
  }

  return router;
}
