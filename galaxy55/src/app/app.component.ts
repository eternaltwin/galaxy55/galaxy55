import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  public players: readonly string[] | null;

  constructor() {
    this.players = null;
  }

  public displayPlayers() {
    this.players = ["Demurgos", "Deepnight", "20000Volts", "MrPapy"];
    console.log("players");
  }
}
