import { User } from "@eternal-twin/etwin-api-types/user/user.js";

export interface PlanetService {
  getPlayersOnPlanet(planetId: string): Promise<User[]>
}
